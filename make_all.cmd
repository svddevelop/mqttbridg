﻿@set LAZ_HOME=Z:\fpcdelux\lazarus\
@set LPI_HOME=.\lpi

copy %LPI_HOME%\linux-arm.lpi .\mqttbridge.lpi
%LAZ_HOME%\lazbuild.exe .\mqttbridge.lpr

copy %LPI_HOME%\linux-aarch64.lpi .\mqttbridge.lpi
%LAZ_HOME%\lazbuild.exe .\mqttbridge.lpr

copy %LPI_HOME%\win64-x64.lpi .\mqttbridge.lpi
%LAZ_HOME%\lazbuild.exe .\mqttbridge.lpr


copy %LPI_HOME%\linux-x64.lpi .\mqttbridge.lpi
%LAZ_HOME%\lazbuild.exe .\mqttbridge.lpr

copy %LPI_HOME%\win64-x64.lpi .\mqttbridge.lpi

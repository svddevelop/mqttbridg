unit u_commonobj;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,   MQTT,
  syncobjs, // TCriticalSection
  fptimer,
  u_configurator,
  u_globals,
  eventlog,
  u_jsonrules
  ;

type

  TTerminatedEvent = function: Boolean of Object;
  { TCommonObj }

  TCommonObj = class
  public
    FLogEvent      : TLogEvent;
    conf:           TConfiguration;
    FOnTerminated:  TTerminatedEvent;
    FOnDestroyObj:  TNotifyEvent;

    MQTTClient:     TMQTTClient;

    SyncCode:       TCriticalSection;
    TimerTick:      TFPTimer;
    cnt:            integer;

    dodebugmsg:     boolean;

    latestPing:     TDateTime;

    property    OnTerminated:TTerminatedEvent read FOnTerminated write FOnTerminated;
    property    OnDestroyObj:TNotifyEvent     read FOnDestroyObj write FOnDestroyObj;
    function    Terminated: Boolean;
    constructor Create(aLogEvent: TLogEvent);
    destructor  Destroy; override;

    procedure OnConnAck(Sender: TObject; ReturnCode: integer);
    procedure OnPingResp(Sender: TObject);
    procedure OnSubAck(Sender: TObject; MessageID: integer; GrantedQoS: integer);
    procedure OnUnSubAck(Sender: TObject);
    procedure OnPublish(Sender: TObject; topic, payload: ansistring; isRetain: boolean);

    procedure OnTimerTick(Sender: TObject);
    procedure DoRun;

    procedure beforeDoRunOnce;
    procedure doRunOnce;
    procedure afterDoRunOnce;

    procedure doOnCreate;
    procedure doOnFree;
  end;

implementation

uses  Crt;

const
  ContrBreakSIG = #$03;

function NewTimer(Intr: integer; Proc: TNotifyEvent; AEnable: boolean = false): TFPTimer;
begin
  Result := TFPTimer.Create(nil);
  Result.UseTimerThread:=false;
  Result.Interval := Intr;
  Result.OnTimer := Proc;
  Result.Enabled := AEnable;
end;


{ TCommonObj }

function TCommonObj.Terminated: Boolean;
begin
  Result := Assigned(OnTerminated);
  if Result then
    Result := OnTerminated();
end;

constructor TCommonObj.Create(aLogEvent: TLogEvent);
begin
  FLogEvent := aLogEvent;
  aLogEvent('TCommonObj.Create 1');
  conf      := TConfiguration.Create;
  conf.FLogEvent := aLogEvent;
  aLogEvent('TCommonObj.Create readCfgFile, cfg==' + BoolToStr(assigned(conf), 1=1));
  if conf.readCfgFile then
    aLogEvent('configuration ok')
  else
    aLogEvent('configuration not ok');
  aLogEvent('TCommonObj.Create end');
end;

destructor TCommonObj.Destroy;
begin
  if Assigned(OnDestroyObj) then
    OnDestroyObj(Self);
  inherited Destroy;
end;

procedure TCommonObj.OnConnAck(Sender: TObject; ReturnCode: integer);
begin
  SyncCode.Enter;
  SyncCode.Leave;
end;

procedure TCommonObj.OnPingResp(Sender: TObject);
begin
  FLogEvent('TCommonObj.OnPingResp');
  SyncCode.Enter;
  SyncCode.Leave;
end;

procedure TCommonObj.OnSubAck(Sender: TObject; MessageID: integer;  GrantedQoS: integer);
begin
  FLogEvent('TCommonObj.OnSubAck');
  SyncCode.Enter;
  SyncCode.Leave;
end;

procedure TCommonObj.OnUnSubAck(Sender: TObject);
begin
  FLogEvent('TCommonObj.OnUnSubAck');
  SyncCode.Enter;
  SyncCode.Leave;
end;

procedure TCommonObj.OnPublish(Sender: TObject; topic, payload: ansistring;
  isRetain: boolean);
begin

end;

procedure TCommonObj.OnTimerTick(Sender: TObject);
begin
  SyncCode.Enter;
  if not MQTTClient.PingReq then
  begin
    globalLogMsg('Ping false');
    //do new reconnect or new subscribe?
  end;
  SyncCode.Leave;
end;

procedure TCommonObj.DoRun;
begin
  doOnCreate;

  //todo: wait 'OnConnAck'
  Sleep(1000);
  if not MQTTClient.isConnected then
  begin
    writeln('connect FAIL');
    exit;
  end;

  // mqtt subscribe to all topics
  {$IFDEF LINUX}
  if FindCmdLineSwitch('r') then
  {$ENDIF}
  MQTTClient.Subscribe('#');

  cnt := 0;
  TimerTick := NewTimer(5000, @OnTimerTick, true);
  try
    while (not Terminated) and (MQTTClient.isConnected) do
    begin
      // wait other thread
      CheckSynchronize(1000);

      (*
      //old: Check for ctrl-c
      if KeyPressed then          //  <--- CRT function to test key press
        if ReadKey = ContrBreakSIG then      // read the key pressed
        begin
          writeln('Ctrl-C pressed.');
          //Terminate;
          Break;
        end;
        *)
    end;

    MQTTClient.Unsubscribe('#');
    MQTTClient.Disconnect;
    Sleep(100);
    MQTTClient.ForceDisconnect;
  finally
    doOnFree;
  end;
end;

procedure TCommonObj.beforeDoRunOnce;
begin
  FLogEvent('TCommonObj.beforeDoRunOnce: 1');
  doOnCreate;
  FLogEvent('TCommonObj.beforeDoRunOnce: 2');

  //todo: wait 'OnConnAck'
  Sleep(1000);
  if not MQTTClient.isConnected then
  begin
    FLogEvent('TCommonObj.beforeDoRunOnce: 3 MQTT connection FAIL');
    writeln('connect FAIL');
    exit;
  end;

  FLogEvent('TCommonObj.beforeDoRunOnce: 5');
  // mqtt subscribe to all topics
  MQTTClient.Subscribe('#');

  FLogEvent('TCommonObj.beforeDoRunOnce: 5');
  cnt := 0;
  TimerTick := NewTimer(5000, @OnTimerTick, true);
  FLogEvent('TCommonObj.beforeDoRunOnce: end');

end;

procedure TCommonObj.doRunOnce;
begin
    // wait other thread
//      CheckSynchronize(1000);
  if ( (now - latestPing) > C_MQTT_PINGPERIOD ) then
  begin
    latestPing := now;
    if MQTTClient.isConnected then
      if not MQTTClient.PingReq then
        FLogEvent(ClassName +'.DoRunOnce:not ping');
  end;
end;

procedure TCommonObj.afterDoRunOnce;
begin
  MQTTClient.Unsubscribe('#');
  FLogEvent('TCommonObj.Unsubscribe');
  MQTTClient.Disconnect;
  FLogEvent('TCommonObj.Disconnect');
  Sleep(100);
  MQTTClient.ForceDisconnect;
  doOnFree;
end;

procedure TCommonObj.doOnCreate;
begin
  FLogEvent('TCommonObj.doOnCreate: Start');
  MQTTClient := conf.createMQTT(0);
  FLogEvent('TCommonObj.doOnCreate: MQTT created');
//  MQTTClient.OnConnAck  := @OnConnAck;
//  MQTTClient.OnPingResp := @OnPingResp;
  //MQTTClient.OnPublish  := @OnPublish;
//  MQTTClient.OnSubAck   := @OnSubAck;
  MQTTClient.Connect();
  FLogEvent('TCommonObj.doOnCreate: 3');
end;

procedure TCommonObj.doOnFree;
begin
  FreeAndNil(conf);
  FreeAndNil(TimerTick);
  FreeAndNil(MQTTClient);
  FreeAndNil(SyncCode);
  FLogEvent('TCommonObj.doOnCreate: MQTT is free');
  Sleep(2000); // wait thread dies
end;

end.


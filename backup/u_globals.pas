unit u_globals;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

const
  c_version = '0.1.0.21112301';
  C_LOG_FILECHANGEPERIOID = 6/24; // 6 files per day
  C_MQTT_PINGPERIOD = 20/(24*60*60); // per 20 Sec

  {$IFDEF UNIX}
  c_exename = '/bin/sh';
  {$ELSE}
  c_exename = 'cmd';
  {$ENDIF}

type
  TLogEvent        = procedure(aMsg: String) of Object;
  TTerminatedEvent = function: Boolean of Object;

var
  global_debug:    Boolean;
  global_LogEvent: TLogEvent;

procedure globalLogMsg( aMsg: String);

implementation

procedure globalLogMsg( aMsg: String);
begin
  if Assigned(global_LogEvent) then
    global_LogEvent(aMsg);
end;

end.


unit u_jsonrules;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpjsonrtti, fpjson, jsonparser, MQTT
  , u_globals;

const
  sodSrc = True;
  sodDst = False;

const
  C_JOFld_Src                      = 'src';
  C_JOFld_Dst                      = 'dst';
  C_JOFld_mqttTopic                = 'mqtt_topic';
  C_JOFld_clause                   = 'clause';
  C_JOFld_clauses                  = 'clauses';
  C_JOFld_path                     = 'path';
  C_JOFld_operation                = 'operation';
  C_JOFld_value                    = 'value';
  C_JOFld_opClause                 = 'op_clause';
  C_JOFld_clausea                  = 'clausea';
  C_JOFld_clauseb                  = 'clauseb';
  C_JOFld_action                   = 'action';
  C_JOFld_actions                  = 'actions';
  C_JOFld_fieldname                = 'fieldname';
  C_JOFld_fieldvalue               = 'fieldvalue';
  C_JOFld_dstaction_if             = 'insertfield';
  C_JOFld_dstaction_cf             = 'copyfield';
  C_JOFld_dstaction_cv             = 'calcvalue';
  C_JOFld_dstaction_exe            = 'execute';
  C_JOFld_dstaction_empty          = 'empty';
  C_JOFld_dstaction_dtype          = 'dtype';


type

  { TRulesProcessor }

  T_SrcOrDst = boolean;

  TRulesProcessor = class(TComponent)
  private
    FOutIsEmpty: Boolean;
  public
    Rule, Inp, Outp: TJSONObject;
    RuleFN, InpFN, OutFN: String;

    MQTTClient: TMQTTClient;

    procedure init;
    function process: Boolean;

    function readRuleFromFile(const aFN: String): Boolean;
    function setInp(const aStrJSonTxt: String): Boolean;
    procedure read_cfg(aRuleFN, aInpFN, aOutFN: String);
    function getRuleSrcTopic: String;
    function getRuleDstTopic: String;
    class function readTextFile(const aFN: String; var aStr: String): Boolean;
    class function readJSONfromFile(const aFN: String; var aObj: TJSONObject): boolean;

    function getOutsetIn( aOutMsg: String): String;

    class function JSONaddChildNode(const aPath: String; var aObj: TJSONObject; aValue: TJSONData): TJSONData;
    class function check_json:String;

    class procedure preparetext(var aStr: AnsiString);
    //class function getValueByCondition(var aSrcCond: TCondRec; var aSrcObj: TJSONObject): TJSONData;
    //class procedure doOperation(var aRule: TRule; var aSrc,aDst: TJSONObject);

  public
    class procedure logMessage(aMsg: String);
  public
    rulesClausesStates: array of Boolean;
    rulesOpClauses: array of Boolean;
    function parseRule( var aRule, aInp, aOut: TJSONObject): boolean;
    function getTopic(var aRule: TJSONObject; const aPart: T_SrcOrDst):String;
    function getOut: String;

    function getSrcClauseCount(var aRule: TJSONObject): Integer;
    function getSrcClause(var aRule: TJSONObject; const aIdx: Integer): TJSONObject;
    function doSrcClauseState(var aRule, aInp: TJSONObject; const aIdx: Integer): Boolean;
    function getSrcOpClauseCount(var aRule: TJSONObject): Integer;
    function fillRulesClauses(var aRule, aInp: TJSONObject): Boolean;
    function fillRulesOpClauses(var aRule: TJSONObject): Boolean;
    function isRulesClausesStatesTrue: Boolean;

    function getDstActionCount( var aRule: TJSONObject): Integer;
    function getDstAction(var aRule: TJSONObject; const aIdx: Integer): TJSONObject;
    function fillOut(var aRule, aOut: TJSONObject): boolean;

    function getPath(const aJSON: TJSONData; const aPath: String; var aResult: TJSONData): boolean;
    function calcValue(const aInp: TJSONData; aFormStr: String; var aOut: String) : Boolean;

    function try2exec(aStr: String): String;

    property OutputIsEmpty: Boolean read FOutIsEmpty;
  end;


implementation

uses
     fileUtil
   , process
   ;

//https://wiki.freepascal.org/Streaming_JSON
//https://wiki.freepascal.org/fcl-json

//https://github.com/cutec-chris/promet-erp/blob/master/promet/source/tools/processdaemon.lpr


{ TRulesProcessor }

procedure TRulesProcessor.init;
begin

  Outp:= TJSONObject.Create;
end;

function TRulesProcessor.process: Boolean;
begin
  Result := False;
  FOutIsEmpty := False;
  if fillRulesClauses(Rule, Inp) then
  begin
     if isRulesClausesStatesTrue then
     begin
        if not Assigned(Outp) then
          Outp := TJSONObject(GetJSON('{}'));
        Result := fillOut(Rule, Outp);
        if Result then
          logMessage('process Output:' + Outp.FormatJSON() )
        else
          logMessage('The output did not ready!');
     end
     else
       logMessage('The rules clauses states are not ready!');
  end
  else
     logMessage('The rules clauses din not parsed!');

end;

function TRulesProcessor.readRuleFromFile(const aFN: String): Boolean;
begin
  Result := readJSONfromFile(aFN, Rule);
end;

function TRulesProcessor.setInp(const aStrJSonTxt: String): Boolean;
begin
  try
     Inp := TJSONObject(GetJSON(aStrJSonTxt));
     Result := Assigned(Inp);
  except on E: Exception do
    begin
        Result := False;
        logMessage('setInp():E:' + E.Message);
    end;
  end;
end;

procedure TRulesProcessor.read_cfg(aRuleFN, aInpFN, aOutFN: String);
begin
  RuleFN := aRuleFN;
  InpFN:=aInpFN;
  OutFN:=aOutFN;

    readJSONfromFile(RuleFN, Rule);
    readJSONfromFile(InpFN, Inp);

end;

function TRulesProcessor.getRuleSrcTopic: String;
begin
  Result := getTopic(Rule, sodSrc);
end;

function TRulesProcessor.getRuleDstTopic: String;
begin
  Result := getTopic(Rule, sodDst);
  preparetext(Result);
end;

class function TRulesProcessor.readTextFile(const aFN: String; var aStr: String): Boolean;
var txt : String;
begin
  Result := FileExists(aFN);
  if Result then

    //aStr := fileutil.ReadFileToString(aFN);
    with TStringList.Create do
    try
      LoadFromFile(aFN);
      aStr := Text;
    finally
      Free;
    end;
end;

class function TRulesProcessor.readJSONfromFile(const aFN: String; var aObj: TJSONObject): boolean;
var txt : String;
    jd: TJSONData;
    fs : TFileStream;
    js : TJSONStringType;
    locParser : TJSONParser;

    procedure preparetxt;
    var
        i: Integer;
    begin
      for i := length(txt) downto 1 do
        if txt[i] < #33 then
          delete(txt, i, 1);
    end;

begin
  Result := readTextFile(aFN, txt);
  if Result then
  try
     //preparetxt;
    js := UTF8Encode(txt);
    locParser := TJSONParser.Create(txt);
    //jd := GetJSON(js);
    jd := locParser.Parse;
    aObj := TJSONObject(jd);
logMessage('readJSONfromFile(' + aFN + '):readed' );
  except on E: Exception do
    begin
        Result := False;
        logMessage('readJSONfromFile(' + aFN + '):E:' + E.Message );
    end;
  end;
end;

function TRulesProcessor.getOutsetIn(aOutMsg: String): String;
begin
  try
    Inp := TJSONObject(GetJSON(aOutMsg));

    //Читать все правила из списка, затем поочереди обрабатывать их, пока обработка не вернет ок


  except on E: Exception do
  end;
end;

class procedure TRulesProcessor.logMessage(aMsg: String);
begin
  //Writeln(aMsg);
  globalLogMsg('   RP:' + aMsg);
end;

function TRulesProcessor.parseRule(var aRule, aInp, aOut: TJSONObject): boolean;
begin
  Result := False;
end;


function TRulesProcessor.getTopic(var aRule: TJSONObject; const aPart: T_SrcOrDst ): String;
begin
  if aPart = sodSrc then
    Result := aRule.GetPath(C_JOFld_Src + '.' + C_JOFld_mqttTopic).AsString
  else
    Result := aRule.GetPath(C_JOFld_Dst + '.' + C_JOFld_mqttTopic).AsString;
end;

function TRulesProcessor.getOut: String;
begin
  Result := outp.FormatJSON();
end;

function TRulesProcessor.getSrcClauseCount(var aRule: TJSONObject): Integer;
var
    clause: TJSONData;
    arrclause: TJSONArray absolute clause;
    path : String;
begin
  Result := 0;
  path := C_JOFld_Src + '.' + C_JOFld_clauses;
  //Writeln(path, ' ',aRule.FormatJSON());
  if getPath(aRule, path, clause) then
    if ( clause.JSONType = jtArray ) then
      Result := arrclause.Count  ;
  logMessage( 'src clauses count ' + IntToStr(Result));
end;

function TRulesProcessor.getSrcClause(var aRule: TJSONObject; const aIdx: Integer): TJSONObject;
var
    p: String;
begin
  p := Format('%s.%s[%d]', [C_JOFld_Src, C_JOFld_clauses, aIdx]);
  Result := TJSONObject(aRule.GetPath( p ));
end;

function TRulesProcessor.doSrcClauseState(var aRule, aInp: TJSONObject; const aIdx: Integer): Boolean;
var
    clause: TJSONObject;
    inpValue, clauseValue: TJSONData;
    iv, cv: TJSONStringType;
begin
  //if global_debug then writeln('doSrcClauseState: Inp:' + aInp.FormatJSON());

  Result := False;
  clause := getSrcClause( aRule, aIdx);
  if Assigned(clause) then
    begin
      getPath(aInp, clause.GetPath(C_JOFld_path).AsString, inpValue );
      getPath(clause, C_JOFld_value, clauseValue);
      if (Assigned(inpValue) and Assigned( clauseValue )) then
        begin
          iv := inpValue.AsString;
          cv := clauseValue.AsString;
          Result := iv = cv;
          logMessage( Format('doSrcClauseState(%d) == %d (%s == %s)', [aIdx, integer(Result), iv, cv]));
          if not Result then
            if global_debug then
              logMessage(' clause:' + clause.FormatJSON());
        end
      else
        if global_debug then
          writeln(' inpvalue or clausevalue did not assigned');

    end;
end;

function TRulesProcessor.getSrcOpClauseCount(var aRule: TJSONObject): Integer;
var
    opclause: TJSONData;
    arrclause: TJSONArray absolute opclause;
begin
  Result := 0;
  if getPath(aRule, C_JOFld_Src + '.' + C_JOFld_opClause, opclause) then
    if ( opclause.JSONType = jtArray ) then
      Result := arrclause.Count  ;
end;

function TRulesProcessor.fillRulesClauses(var aRule, aInp: TJSONObject): Boolean;
var
    i,j : Integer;
begin
   j := getSrcClauseCount(aRule);
   Result := j > 0;
   SetLength(rulesClausesStates, j);
   for i := 0 to j-1 do
     rulesClausesStates[i] := doSrcClauseState(aRule, aInp, i);

end;

function TRulesProcessor.fillRulesOpClauses(var aRule: TJSONObject): Boolean;
var
    i,j : Integer;
    p : String;
    aidx, bidx: Integer;
    ca,cb,op: TJSONData;

    function checkRangeIndex(aIdx: Integer): Boolean;
    begin
      Result := aIdx >= 0;
      if Result then
          Result := aIdx < length(rulesClausesStates);
    end;

begin
  j := getSrcOpClauseCount( aRule );
  Result := j > 0;
  SetLength(rulesOpClauses, j);
  for i := 0 to j-1 do
    begin
        p := Format('%s.%s[%d]',[C_JOFld_Src, C_JOFld_opClause, i]);
        getPath(aRule, p + '.' + C_JOFld_clausea, ca);
        getPath(aRule, p + '.' + C_JOFld_clauseb, cb);
        getPath(aRule, p + '.' + C_JOFld_operation, op);
        Result := Assigned(ca);
        if Result then
          Result := Assigned(cb);
        if Result then
          Result := Assigned(op);
        if Result then
          begin
            aidx := ca.AsInteger;
            Result := checkRangeIndex(aidx);
          end;
        if Result then
          begin
            bidx := cb.AsInteger;
            Result := checkRangeIndex(bidx);
          end;
         if Result then
           begin
             if op.AsString = '==' then
               ;
           end;

    end;

end;

function TRulesProcessor.isRulesClausesStatesTrue: Boolean;
var i : Integer;
begin
  Result := True;
  for i := 0 to length(rulesClausesStates) -1 do
     Result := Result and rulesClausesStates[i];
  logMessage( 'src clauses states is  ' + BoolToStr(Result, 1=1));
end;

function TRulesProcessor.getDstActionCount(var aRule: TJSONObject): Integer;
var
    actions: TJSONData;
    arractions: TJSONArray absolute actions;
begin
  Result := 0;
  if getPath(aRule, C_JOFld_Dst + '.' + C_JOFld_actions, actions) then
    if ( actions.JSONType = jtArray ) then
      Result := arractions.Count  ;
  logMessage( 'dst actions count ' + IntToStr(Result));
end;

function TRulesProcessor.getDstAction(var aRule: TJSONObject; const aIdx: Integer): TJSONObject;
var
    p: String;
begin
  p := Format('%s.%s[%d]', [C_JOFld_Dst, C_JOFld_actions, aIdx]);
  Result := TJSONObject(aRule.GetPath( p ));
end;

function TRulesProcessor.fillOut(var aRule, aOut: TJSONObject): boolean;
var
    i,j,l,valInt             : Integer;
    p, actn, sp, dp, dts     : String;
    fn,fv,ac,dt              : TJSONData;
    locParser                : TJSONParser;
    empty                    : Boolean;
    valFloat                 : Double;

    procedure try_to_convert_value;
    begin
      //LogMessage(' >< Path:' + p + C_JOFld_Dst + C_JOFld_dstaction_dtype);
       //LogMessage(' >< Assigned(dt):' + BoolToStr(Assigned(dt), 1=1));
       dts := LowerCase(dt.AsString);    // logMessage('action:'+ actn+ '>>'+ sp+ '>>0 convert to ' + dts );
       l := length(sp);
       if ( l > 0 ) then
         if (length(dts) >0) then
           if    ( dts[1] = 'n') //numeric
               or( dts[1] = 'i') //integer
           then
         begin
           logMessage('action:'+ actn+ '>>'+ sp+ '>> convert to ' + dts );
           //fv.AsInt64 := fv.AsInt64;

           if (sp[l] = '"') then delete(sp, l, 1);
           if (sp[1] = '"') then delete(sp, 1, 1);

           if ( dts[1] = 'i') then
             if TryStrToInt(sp, valInt) then
             begin
               logMessage('action:'+ actn+ '>>'+ sp+ '>> Integer ' + IntToStr(valInt) );
               fv := CreateJSON( valInt );
             end;

           if ( dts[1] = 'n') then
             if TryStrToFloat(sp, valFloat) then
             begin
               logMessage('action:'+ actn+ '>>'+ sp+ '>> Numeric ' + FloatToStr(valInt) );
               fv := CreateJSON( valFloat );
             end;

         end;
    end;

begin
  try
  j := getDstActionCount( aRule );
  Result := j > 0;
  SetLength(rulesOpClauses, j);
  if Result then
    logMessage('fillOut: actions count:' + IntToStr(j));
  for i := 0 to j-1 do
    begin
        p := Format('%s.%s[%d]',[C_JOFld_Dst, C_JOFld_actions, i]);
        SetLength(actn, 0);
        logMessage('fillOut idx:'+IntToStr(i)+ ' '+ p );
        p := p + '.';
        Result := getPath(aRule, p + C_JOFld_action, ac);
        if Result then
        begin
          actn := ac.AsString;
          logMessage('rule action:' + ac.FormatJSON(AsCompactJSON) );
        end;

        if actn = C_JOFld_dstaction_if then   //insert filed
          begin
            Result := Result and getPath(aRule, p + C_JOFld_fieldname, fn);
            Result := Result and getPath(aRule, p + C_JOFld_fieldvalue, fv);
            getPath(aRule, p + C_JOFld_dstaction_dtype, dt);
            if Result then
              begin
                   //try_to_convert_value;
                   logMessage('action:'+ actn+ '>>'+ fn.AsString+ '='+ fv.AsString );
                   JSONaddChildNode(fn.AsString, aOut, fv );
              end
            else
              logMessage('The action is incorrect');
          end;
        if actn = C_JOFld_dstaction_cf then    //copy value
          begin
            //Writeln(aRule.FormatJSON());
            //Writeln(p + C_JOFld_Dst + C_JOFld_fieldname);
            //logMessage('rule action:' + p);
            //logMessage( ac.FormatJSON() );
            Result := Result and getPath(aRule, p + C_JOFld_Src + C_JOFld_fieldname, fn);
            Result := Result and getPath(aRule, p + C_JOFld_Dst + C_JOFld_fieldname, fv);
            getPath(aRule, p + C_JOFld_dstaction_dtype, dt);
            if Result then
              begin
                sp := fn.AsString;
                dp := fv.AsString;
                if getPath( Inp, sp, fv) then
                  begin
                    try_to_convert_value;
                    logMessage('action:'+ actn+ '>>'+ sp+ '>>'+ dp+ '=:'+ fv.AsString +#13#10);
                    JSONaddChildNode(dp, aOut, fv );
                  end
                else
                  logMessage('The path "' + sp + '" did not fount in Input');
              end
            else
              logMessage('Rule.Action is incorrect ' + ac.FormatJSON(AsCompactJSON));
          end;

        if actn =  C_JOFld_dstaction_cv then   //calc value
          begin
            dt := TJSONObject.Create;
            Result := Result and getPath(aRule, p + C_JOFld_Src + C_JOFld_fieldvalue, fn);
            Result := Result and getPath(aRule, p + C_JOFld_Dst + C_JOFld_fieldname, fv);
                                 getPath(aRule, p + C_JOFld_dstaction_dtype, dt);
            if Result then
              begin
                sp := fn.AsString;
                dp := fv.AsString;
                if calcValue( Inp, sp, sp) then
                  begin
                    fv.AsString:= sp;

                    logMessage('action:'+ actn+ '>>Value='+ sp+ '<< >>'+ dp+ '=:'+ fv.AsString );

                    try_to_convert_value;

                   JSONaddChildNode(dp, aOut, fv );
                   logMessage('');
                  end
                else
                  logMessage('The path "' + sp + '" did not fount in Input');
              end
            else
              logMessage('Rule.Action is incorrect ' + ac.FormatJSON(AsCompactJSON));
          end;

        // ! Warning: this cut did not tested
        if actn =  C_JOFld_dstaction_exe then
          begin
            FOutIsEmpty := True;
            if  getPath(aRule, p + C_JOFld_Src + C_JOFld_dstaction_empty, fn) then
              FOutIsEmpty := fn.AsBoolean;

            Result := Result and getPath(aRule, p + C_JOFld_Dst + C_JOFld_dstaction_exe, fv);
            if Result then
              begin
                sp := fv.AsString;
                if calcValue( Inp, sp, sp) then
                  begin
                    //fv.AsString:= sp;
                    logMessage('action:'+ actn+ '>>'+ sp );
                    sp := try2exec( sp );

                    logMessage('action:'+ actn+ '<<'+ sp );

                    if not FOutIsEmpty then
                    try
                      locParser := TJSONParser.Create(sp);
                      aOut := TJSONObject(locParser.Parse);
                    except on E : Exception do
                      logMessage('action:'+ actn+ ':E:'+ E.Message );
                    end;

                    //if Assigned(MQTTClient) then
                    //  MQTTClient.Publish(aRule.GetPath('dst.mqtt_topic').AsString, sp);


                    //JSONaddChildNode(dp, aOut, fv );
                    logMessage('');
                  end
                else
                  logMessage('The path "' + sp + '" did not fount in Input');
              end
            else
              logMessage('Rule.Action is incorrect ' + ac.FormatJSON(AsCompactJSON));
          end;
        //**

        if not Result then
          begin
            Result := getPath(aRule, p + C_JOFld_fieldname, fn);
          end;
    end;

  except on E : Exception do
    begin
        Result := False;
        logMessage('fillOut:E:' + E.Message);
        logMessage('fillOut:aRule:' + aRule.FormatJSON());
        logMessage('fillOut:aOut' + aOut.FormatJSON());

    end;
  end;
end;

function TRulesProcessor.getPath(const aJSON: TJSONData; const aPath: String;
  var aResult: TJSONData): boolean;
begin
  aResult := aJSON.FindPath(aPath);
  Result := Assigned(aResult);
end;

function TRulesProcessor.calcValue(const aInp: TJSONData; aFormStr: String;var aOut: String): Boolean;
var
    i,j : Integer;
    fld: String;
    fv : TJSONData;
    ok : Boolean;
begin
  Result := True;
  i := pos('{', aFormStr);
  while i > 0 do
  begin
     j := pos('}', aFormStr, i);
     if ( j <= i) then Result := False;

     if Result then
     begin
       fld := copy(aFormStr, i+1, j-i-1);
       delete(aFormStr, i, j-i+1);
       ok := getPath(aInp, fld, fv);
       if (not ok)or(not Assigned(fv)) then
         Result := False
       else
         insert(fv.AsString, aFormStr, i);
     end;
     if not Result then Exit(False);
     i := pos('{', aFormStr);
  end;
  aOut := aFormStr;
end;

function TRulesProcessor.try2exec(aStr: String): String;
var
    i : Integer;
    exename, outstr: String;
begin
  Result := '{}';
  exename := c_exename;
  //try
    try
      if not RunCommand( exename
                  , ['-c', aStr ]
                  , outstr) then
        logMessage(ClassName+':Error! The program could not to run "'+aStr+'" command.')
      else
        Result := outstr;

    except on E : Exception do
    end;

  //finally
  //end;
end;


class function TRulesProcessor.JSONaddChildNode(const aPath: String; var aObj: TJSONObject; aValue: TJSONData): TJSONData;
var
  sa: TStringArray;
  i,j  : Integer;
  node, parent : TJSONData;
  obj : TJSONObject absolute parent;
begin
  sa := aPath.Split(['.']);
  Result := nil;
  parent := aObj;
  i := 0;
  while (i < length(sa)) do
  begin
      node := obj.FindPath(sa[i]);
      if not Assigned( node ) then
      begin
         j := obj.Add( sa[i], aValue );
         node := obj.Items[j];
      end;
      parent := TJSONObject(node);
      inc(i);
  end;
  Result := Node;
end;

class function TRulesProcessor.check_json:String;
var
  obj,itm : TJSONData;
  json : String;
begin
  json:=
  '{"user":'+
    '{    "userid": 1900,'+
        '"username": "jsmith",'+
        '"password": "secret",'+
        '"groups": [ "admins", "users", "maintainers"]'+
    '}'+
  '}';
  obj := GetJSON(json);
  try
    try
      //Result := obj.FindPath('user.groups[3]').AsString;
      itm := obj.GetPath('user.groups[2]');
      Result := itm.AsJSON;
      if obj.JSONType = jtObject then
      begin
        Result += ' obj';
        //добавляю новую ноду
        itm := JSONaddChildNode('user.tag', TJSONObject(obj),  CreateJSON('123a') );

        //TJSONObject(obj).Extract('user.tag').AsString:= '123';

        Result += #13#10 + obj.FormatJSON();
      end;
    except
        itm := nil;
    end;
  finally
    obj.Free;
  end;


end;

class procedure TRulesProcessor.preparetext(var aStr: AnsiString);
var
    i: Integer;
begin
  for i := length(aStr) downto 1 do
    if aStr[i] < #33 then
      delete(aStr, i, 1);
end;

(*class function TRulesProcessor.getValueByCondition(var aSrcCond: TCondRec; var aSrcObj: TJSONObject): TJSONData;
begin
  Result := nil;

  if ( aSrcCond.condition = dcNo ) then
    Result := aSrcObj.GetPath(aSrcCond.tag);
end;*)

(*class procedure TRulesProcessor.doOperation(var aRule: TRule; var aSrc,aDst: TJSONObject);
var
  sitm, ditm: TJSONData;
begin
  if ( aRule.op = doCopyFromSrc ) then
  begin
    sitm := getValueByCondition( aRule.Src, aSrc );
    if Assigned(sitm) then
      JSONaddChildNode( aRule.Dst.tag, aDst, sitm);
  end;

end;*)


end.


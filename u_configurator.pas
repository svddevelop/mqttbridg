unit u_configurator;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpjsonrtti, fpjson, jsonparser, MQTT, u_jsonrules, u_globals;


type

  { TConfigJSONData }

  TConfigJSONData = class helper for TJSONData
  private
    function getTryValue(const aPath: String): TJSONData;
    function getMQTTHost: String;
    function getMQTTPort: word;
    function getRulesDir: String;
    function getTopicIn: String;
    function getTopicOut: String;
  public
    property mqtthost: String read getMQTTHost;
    property mqttport: word   read getMQTTPort;
    property topicin: String  read getTopicIn;
    property topicout: String read getTopicOut;
    property rulesdir: String read getRulesDir;
  end;

  { TConfiguration }

  TConfiguration = class
  public
    FLogEvent : TLogEvent;
    cfg : TJSONArray;
    clients: array of TMQTTClient;
    RulesProcessor: TRulesProcessor;

    procedure conAddNewConfigurationDlg;
    procedure conListConfurationsDlg;
    procedure conRemoveConfigurationDlg;

    procedure conInstallAsDaemonDlg;

    procedure conEditRuleDlg(const aFN: String);
    function  conRuleDlgSetCond(var aFld,aVal: String): Boolean;

    function  conValidateJsonFile(const aFN: String): Boolean;

    function  getFullConfigurationFile: String;
    function  readCfgFile: Boolean;
    function  writeCfgFile: Boolean;

    function  createMQTT(const aIdx: Integer = 0): TMQTTClient;
    procedure OnPublish(Sender: TObject; topic, payload: ansistring; isRetain: boolean);
    function  getMQTTClientIndex(aClient: TMQTTClient): Integer;

    procedure  log(aMsg: String);

    constructor Create;
    destructor Destroy;override;
  end;

implementation

uses
    process
  , fileutil
    ;

const
  c_mqtthost = 'mqtt_host';
  c_mqttport = 'mqtt_port';
  c_topicin  = 'topic_in';
  c_topicout = 'topic_out';
  c_rulesdir = 'rules_dir';
  c_def_mqttport = 1883;
  c_cfg_dir    = '.mqttbridge';
  c_fs_sep = {$IFDEF WINDOWS}'\'{$ELSE}'/'{$ENDIF};


{ TConfiguration }

procedure TConfiguration.conAddNewConfigurationDlg;
var
  newcfg: TJSONObject;
  n: TJSONData;
  s : String;
  i : Integer;
begin
  newcfg := TJSONObject.Create;
  Writeln('Add new confuguration:');
  Write('  get the mqtt server host name or IP: ');
  Readln( s );
  n := CreateJSON(s); TRulesProcessor.JSONaddChildNode(c_mqtthost, newcfg, n);

  Write('  get the mqtt server port (default must be 1883): ');
  Readln( s );  if not TryStrToInt(s, i) then
    i := c_def_mqttport;
  if not((i > 0)and(i < 65536)) then i := c_def_mqttport;
  n := CreateJSON(i); TRulesProcessor.JSONaddChildNode(c_mqttport, newcfg, n);

  Write('  get the input topic: ');
  Readln( s );
  n := CreateJSON(s); TRulesProcessor.JSONaddChildNode(c_topicin, newcfg, n);

  Write('  get the output topic: ');
  Readln( s );
  n := CreateJSON(s); TRulesProcessor.JSONaddChildNode(c_topicout, newcfg, n);

  Write('  get the path to rules: ');
  Readln( s );
  n := CreateJSON(s); TRulesProcessor.JSONaddChildNode(c_rulesdir, newcfg, n);
  if not DirectoryExists(s) then
  begin
    Writeln(' Warning: the "' + s +' did not exists!');
    ForceDirectories(s);
  end;

  //if not Assigned(cfg) then
  //    Writeln('cfg is not assined');


  if not assigned(cfg) then
    cfg := CreateJSONArray([]);

  cfg.Add(newcfg);
  writeCfgFile;
end;

procedure TConfiguration.conListConfurationsDlg;
var
  i,j : Integer;
begin
  j := cfg.Count;
  for i := 0 to j-1 do
   begin
     Writeln('[',i+1,']:');
     Writeln(cfg.Items[i].FormatJSON() );

   end;

end;

procedure TConfiguration.conRemoveConfigurationDlg;
var
   i : Integer;
begin
  Writeln(Format('The configuratin have %d entities. Which can you drop? ', [cfg.Count]));
  Readln(i);
  if  ( ( i > 0 ) and (i <= cfg.Count)) then
      cfg.Delete(i -1);
  writeCfgFile;
end;

procedure TConfiguration.conInstallAsDaemonDlg;

//
// https://wiki.lazarus.freepascal.org/Daemons_and_Services
//

var
  exename, outstr: String;
  cmds: TStringList;
  arrcmds: array of AnsiString;
var
  i : Integer;
begin
  exename := c_exename;
  cmds := TStringList.Create;
  cmds.Add('[Unit]');
  cmds.Add('Description=mqtt bridge daemon');
  cmds.Add('After=network.target');
  cmds.Add('');
  cmds.Add('[Service]');
  cmds.Add('Type=simple');
  cmds.Add('ExecStart=/bin/mqttbridge.bin -r');
  cmds.Add('RemainAfterExit=yes');
  cmds.Add('TimeoutSec=25');
  cmds.Add('');
  cmds.Add('[Install]');
  cmds.Add('WantedBy=multi-user.target');
  cmds.SaveToFile('/tmp/mqttbridge.service') ;

  cmds.clear;
  cmds.add('sudo cp /tmp/mqttbridge.service /lib/systemd/system/mqttbridge.service');
  cmds.add('sudo rm -f /tmp/mqttbridge.service');
  cmds.add('sudo cp ' + exename + ' /bin/mqttbridge.bin');
  cmds.add('sudo systemctl daemon-reload');
  cmds.add('sudo systemctl enable --now mqttbridge');
  cmds.add('sudo systemctl status mqttbridge');
  //cmds.add('');


  Writeln('Warning: the program must be run as sudo user or root!');
  try
    for i := 0 to cmds.count-1 do
    if not
      RunCommand( exename
                  , ['-c', cmds[i] ]
                  , outstr) then
      log(ClassName +':Error! The program could not to run "'+cmds[i]+'" command.')
    else
      Writeln(outstr);
  finally
    cmds.Free;
  end;
  {$IFDEF LINUX}
  {$ENDIF}
end;

procedure TConfiguration.conEditRuleDlg(const aFN: String);
var
  jrule: TJSONObject;

begin
  //'set name of field';
  //'set';
end;

function TConfiguration.conRuleDlgSetCond(var aFld, aVal: String): Boolean;
begin
  Result := False;
end;

function TConfiguration.conValidateJsonFile(const aFN: String): Boolean;
var
  jd: TJSONData;
  txt : String;
  locParser : TJSONParser;
begin
  Result := TRulesProcessor.readTextFile(aFN, txt);
  if Result then
  try
    //js := UTF8Encode(txt);
    locParser := TJSONParser.Create(txt);
    jd := locParser.Parse;
    Writeln(jd.FormatJSON());

  except on E: Exception do
    begin
        Result := False;
        WriteLn('conValidateJsonFile(' + aFN + '):E:' + E.Message );
    end;
  end;
end;

function TConfiguration.getFullConfigurationFile: String;
var
  cfgfn : String;
  cfgdir: String;
begin
  //if  global_debug then writeln('getFullConfigurationFile:');
  {$IFDEF WINDOWS}
  cfgfn := ChangeFileExt(ExtractFileName(ParamStr(0)), '.conf');
  cfgdir := ExtractFilePath(ParamStr(0)) + c_cfg_dir + c_fs_sep;
  {$ELSE}
  cfgfn := 'mqttbridge.conf';
  cfgdir := '/etc/';
  {$ENDIF}
  Result := cfgdir + cfgfn;
  //if  global_debug then writeln(Result);
end;

function TConfiguration.readCfgFile: Boolean;
var
  fn : String;
  jo : TJSONObject;
begin
  FLogEvent('conf.readCfgFile: start');
  Result := False;
  if global_debug then Writeln('readCfgFile');
  fn := getFullConfigurationFile();
  FLogEvent('conf.readCfgFile:' + fn);
  jo := TJSONObject(cfg);
  if FileExists( fn ) then
  try
    Result:= TRulesProcessor.readJSONfromFile(fn, jo);
    //if  global_debug then if not Result then Writeln('false == TRulesProcessor.readJSONfromFile');
    cfg := TJSONArray(jo);
    if global_debug then FLogEvent('cfg:' + cfg.FormatJSON());
    if not assigned(cfg) then
      cfg := CreateJSONArray([]);
  except on E: Exception do
    begin
      Result := True;
      FLogEvent (fn);
      FLogEvent ('Exception on read configuration :' + E.Message);
      if not assigned(cfg) then
        cfg := CreateJSONArray([]);
    end
  end
  else
    try
      with TFileStream.Create(fn, fmCreate) do
      try
        Result := True;
      finally
        Free;
      end;
    except on E: Exception do
       FLogEvent( fn+#13#10+'Exception on write configuration'+#13#10+E.Message);
    end;
end;

function TConfiguration.writeCfgFile: Boolean;
var
  fn, js : String;
begin
  Writeln( 'writeCfgFile:');
  fn := getFullConfigurationFile();
  Writeln('Save to ', fn);
  js := cfg.FormatJSON();
  with TFileStream.Create(fn, fmCreate) do
  try
    Write( pointer(js)^, length(js));
  finally
    Free;
  end;
end;

function TConfiguration.createMQTT(const aIdx: Integer): TMQTTClient;
var
  loccfg : TJSONData;
  i : Integer;
begin
  loccfg := cfg.Items[aIdx];
  Result := TMQTTClient.Create(
              loccfg.FindPath(c_mqtthost).AsString
              , loccfg.FindPath(c_mqttport).AsInteger
              );
  Result.OnPublish  := @OnPublish;
  i := Length(Self.clients);
  SetLength(clients, i+1);
  clients[i] := Result;
end;

procedure TConfiguration.OnPublish(Sender: TObject; topic, payload: ansistring;isRetain: boolean);
var
  i,j            : Integer;
  RulesFL        : TStringList;
  canprocess     : Boolean;
  mqttClient     : TMQTTClient absolute Sender;
  dir,efn        : String;
  pushtopic      : String;
begin
  i := getMQTTClientIndex(mqttClient);
  if (i >= 0) then
  begin
    FLogEvent('conf.OnPublish:'
      + Format('[%d]topic:%s, payload:%s', [i, topic, payload])
    );
    dir := cfg.Items[i].getRulesDir;
    //if (cfg.Items[i].getTopicIn = topic) then
      if RulesProcessor.setInp( payload ) then
      try
        RulesFL := TStringList.Create;
        FindAllFiles(RulesFL, dir, 'Rule*.json;rule*.json', true);
        //FLogEvent('rules:'+dir+' '+ RulesFL.text);
        for j := 0 to RulesFL.Count -1 do
        begin
          efn := ExtractFileName(RulesFL[j]);
          FLogEvent(Format('  rule[%d]:%s',[j, efn]));
          if RulesProcessor.readRuleFromFile(RulesFL[j]) then
          begin
            if (RulesProcessor.getRuleSrcTopic() = topic) then
            begin
              //   FLogEvent('  publish:payload <<' + payload);
              RulesProcessor.init;

              if RulesProcessor.process then
              begin
                FLogEvent('  !process ok');
                if Assigned(RulesProcessor.Outp) then
                begin
                  FLogEvent('  !rule is ok');
                  pushtopic := RulesProcessor.getRuleDstTopic();
                  FLogEvent( efn + ' >> topic:' + pushtopic);
                  mqttClient.Publish(
                        pushtopic
                      , RulesProcessor.Outp.FormatJSON()
                      );
                  FLogEvent('');
                end;
              end;


            end;
          end
          else
            FLogEvent('the file did not readed');
          FLogEvent('');

        end;

      finally
        RulesFL.Free;
      end;
    end;
end;


destructor TConfiguration.Destroy;
begin
  RulesProcessor.Free;
  inherited Destroy;
end;


function TConfiguration.getMQTTClientIndex(aClient: TMQTTClient): Integer;
var
  i : Integer;
begin
  Result := -1;
  for i := 0 to length(clients)-1 do
   if clients[i] = aClient then
     Exit(i);
end;

procedure TConfiguration.log(aMsg: String);
begin
  if Assigned(FLogEvent) then
     FLogEvent(aMsg)
  else
     Writeln(aMsg);
end;

constructor TConfiguration.Create;
begin
  inherited Create;
  cfg := CreateJSONArray([]);
  RulesProcessor := TRulesProcessor.Create(nil);
  //RulesProcessor.logMessage();
end;


{ TConfigJSONData }

function TConfigJSONData.getTryValue(const aPath: String): TJSONData;
begin
  Result := nil;
  try
    Result := FindPath(aPath);
  except on E:Exception do
    Result := nil;
  end;
end;

function TConfigJSONData.getMQTTHost: String;
var
  d: TJSONData;
begin
  d := getTryValue(c_mqtthost);
  if Assigned(d) then
    Result := d.AsString;
end;

function TConfigJSONData.getMQTTPort: word;
var
  d: TJSONData;
begin
  d := getTryValue(c_mqttport);
  if Assigned(d) then
    Result := d.AsInteger;
end;

function TConfigJSONData.getRulesDir: String;
var
  d: TJSONData;
begin
  d := getTryValue(c_rulesdir);
  if Assigned(d) then
    Result := d.AsString;
end;

function TConfigJSONData.getTopicIn: String;
var
  d: TJSONData;
begin
  d := getTryValue(c_topicin);
  if Assigned(d) then
    Result := d.AsString;
end;

function TConfigJSONData.getTopicOut: String;
var
  d: TJSONData;
begin
  d := getTryValue(c_topicout);
  if Assigned(d) then
    Result := d.AsString;
end;

end.



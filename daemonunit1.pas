unit DaemonUnit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, DaemonApp, eventlog, u_commonobj, u_globals;

type

  { TDaemon1 }
  TDaemon1 = class(TDaemon)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleExecute(Sender: TCustomDaemon);
    procedure DataModuleShutDown(Sender: TCustomDaemon);
    procedure DataModuleStart(Sender: TCustomDaemon; var OK: boolean);
    procedure DataModuleStop(Sender: TCustomDaemon; var OK: boolean);
  public
    class var logFileDate: TDateTime;
    ComObj   : TCommonObj;
    FLog     : TEventLog;
    need2stop: Boolean;
    function  getTerminated: Boolean;
    procedure comobjDestroy(Sender: TObject);
    procedure log_Message( aMsg: String);
  public
    class function get_log_file_name: String;
    class procedure init_with_log;
  end;

var
  Daemon1: TDaemon1;
  Log: TextFile;

implementation

procedure RegisterDaemon;
begin
  RegisterDaemonClass(TDaemon1);
end;

{$R *.lfm}

{ TDaemon1 }

procedure TDaemon1.DataModuleCreate(Sender: TObject);
var
    swtch_r, aok: Boolean;
begin
  //if not FindCmdLineSwitch('r') then
  //  Writeln('more info ....');
 u_globals.global_LogEvent := @log_Message;

 {$ifdef WINDOWS}
 swtch_r := False;
 if FindCmdLineSwitch('r') then
    swtch_r := True;
 if FindCmdLineSwitch('-run') then
    swtch_r := True;
 if swtch_r then
    begin
      Self.DataModuleStart(Self, aok);

      Self.Status := csRunning;
      DataModuleExecute(Self);

      Self.DataModuleStop(Self, aok);
    end;
 {$endif}
end;

procedure TDaemon1.DataModuleExecute(Sender: TCustomDaemon);
begin
  (*
  while Self.Status = csRunning do
  begin
    sleep(1000);
    WriteLn(Log, datetimetostr(Now));
    Flush(Log);
    log_Message(DateTimeToStr(Now));
  end;
  *)
  Writeln(Log, 'mqttbridge execute');
  LogMessage('log mqttbridge execute');

  while Self.Status = csRunning do
    begin
      //CheckSynchronize(1000);
      ComObj.doRunOnce;
      Flush(Log);
      Sleep(50);

      if ( (now - logFileDate) > C_LOG_FILECHANGEPERIOID) then
        begin
             CloseFile(Log);
             init_with_log;
        end;
    end;

  ComObj.afterDoRunOnce;
  Writeln(Log, 'mqttbridge execute out');

end;

procedure TDaemon1.DataModuleShutDown(Sender: TCustomDaemon);
begin
  WriteLn(Log, ' Shutdown');
  Flush(Log);
end;

procedure TDaemon1.DataModuleStart(Sender: TCustomDaemon; var OK: boolean);
var
   i : Integer;
begin
  OK := True;
  WriteLn(Log, 'Start');
  Flush(Log);
  need2stop := False;
  OK := not Assigned(comobj);
  If OK then
    try
      i := 95;
      comobj                := TCommonObj.Create(@log_Message);
      ComObj.beforeDoRunOnce;
      i := 97;
      comobj.OnTerminated   := @getTerminated;
      i := 99;
      ComObj.OnDestroyObj   := @comobjDestroy;
      i := 101;
      ComObj.conf.FLogEvent := @log_Message ;
      WriteLn(Log, 'service start');
      i := 104;
    except on E: Exception do
      WriteLn(Log, 'service start except:' + E.Message, ' i:', i);
    end;

  Flush(Log);
end;

procedure TDaemon1.DataModuleStop(Sender: TCustomDaemon; var OK: boolean);
var
  i : Integer;
begin
  OK := True;
  WriteLn(Log, 'Stop');
  Flush(Log);
  need2stop := True;
  Sleep(1000);
  If Assigned(comobj) then
    begin
    // Wait at most 5 seconds.
    While Assigned(comobj) and (I<50) do
      begin
      Sleep(100);
      ReportStatus;
      end;
    end;
  OK := not Assigned(ComObj);
  Flush(Log);
end;

function TDaemon1.getTerminated: Boolean;
begin
  Result := need2stop;
end;

procedure TDaemon1.comobjDestroy(Sender: TObject);
begin
   WriteLn(Log, 'daemon.comobjDestroy');
  if ComObj = Sender then
      ComObj := nil;
end;

procedure TDaemon1.log_Message(aMsg: String);
begin
  WriteLn(Log, FormatDateTime('hh:nn:ss,zzz- ', Now) + aMsg);
  Flush(Log);
end;

class function TDaemon1.get_log_file_name: String;
var
  fl: TStringList;
  dc, cd : TDateTime;

  function getLogFileCreationDate(const aFN: String): TDateTime;
  var FHandle: integer;
  begin
    FHandle := FileOpen(aFN, 0);
    try
      Result := FileDateToDateTime(FileGetDate(FHandle));
    finally
      FileClose(FHandle);
    end;
  end;
begin
  logFileDate := Now;
  {$IFDEF UNIX}
  Result := '/tmp/mqttbridge_' + FormatDateTime('yymmdd_hhnnss', now) + '.log';
  {$ELSE}
  Result := 'mqttbridge_' + FormatDateTime('yymmdd_hhnnss', now) + '.log';
  {$ENDIF}
  fl := TStringList.Create;
  try
    FindAllFiles(fl, ExtractFilePath(Result), 'mqttbridge*.log', true);
    cd := Now -1;
    while fl.Count > 0 do
      begin
        dc := getLogFileCreationDate(fl[0]);
        if ( dc < cd ) then
          DeleteFile(fl[0]);
        fl.Delete(0);
      end;
  finally
    fl.free;
  end;
end;

class procedure TDaemon1.init_with_log;
begin
  AssignFile(Log, get_log_file_name);
  Rewrite(Log);
  Writeln(Log, 'Ver:' + c_version);
end;


initialization
  RegisterDaemon;
  TDaemon1.init_with_log;

end.



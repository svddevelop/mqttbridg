Program mqttbridge;

Uses
{$IFDEF UNIX}
  CThreads,
{$ENDIF}
  DaemonApp, lazdaemonapp, DaemonMapperUnit1, DaemonUnit1, SysUtils,
  MQTT, MQTTReadThread, u_commonobj, u_configurator, u_jsonrules, u_globals;

const
   c_cmdopt_validatejson = '-validate-file';


var
  swtch_r : Boolean;
  conf    : TConfiguration;
  fn      : String;

{$R *.res}

begin
  if FindCmdLineSwitch('h') then
  begin
    Writeln('(C) svddevelop 2021 (https://bitbucket.org/svddevelop/mqttbridg/src/master/).');
    Writeln('MQTTBrocker Ver:' + c_version);
    Writeln('');
    Writeln('Usage: ');
    Writeln(#9'-h : this help message;');
    Writeln(#9'--add-conf : this start the dialog to create of the bridge configuration;');
    Writeln(#9'-r, --run : run as console application.');
    {$ifdef Linux}
    Writeln(#9'--service-install : on Linux install as daemon(service)');
    Writeln(#9#9' and control it with ''service mqttbridge start|stop|status'';');
    {$endif}
    Writeln();
    Writeln();

    Halt(0);
  end;

  if FindCmdLineSwitch('-add-conf') then
  begin
     conf := TConfiguration.Create();
     try
       conf.conAddNewConfigurationDlg();
     finally
       conf.Free();
       Halt(0);
     end;
  end;

  {$ifdef Linux}
  if FindCmdLineSwitch('-service-install') then
  begin
     conf := TConfiguration.Create();
     try
       conf.conInstallAsDaemonDlg();
     finally
       conf.Free();
       Halt(0);
     end;
  end;
  {$endif}

  if FindCmdLineSwitch(c_cmdopt_validatejson) then
  begin
     fn := Application.GetOptionValue(c_cmdopt_validatejson);
     if FileExists(fn) then
     begin
       conf := TConfiguration.Create();
       try
         if conf.conValidateJsonFile(fn) then
           WriteLn('VALID')
         else
           WriteLn('INVALID');
       finally
         conf.Free();
         Halt(0);
       end;

     end
     else
       Writeln(fn, ' file not found');
  end;

  Application.Initialize;

  swtch_r := False;
  if FindCmdLineSwitch('r') then
     swtch_r := True;
  if FindCmdLineSwitch('-run') then
     swtch_r := True;
  if not swtch_r then
  begin



  end;
  Application.Run;
end.

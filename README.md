MQTT Bridge - the daemon for communication between IoT-devices with other devices or application.
The many IoT-devices can not change some paramether like a index for MQTT requests. This service can help to do it "on-the-fly".

The service subscribed for all topics of defined MQTT-brocker, received some message, checked it with extertnal rules,
changed the outgoing message and publishe to the topic.

Before use to need to install the service in the system. You can do it start the application in console with:
`$mqttbridge --install-service`

After it you need to fill the configuration file (for Linux this is '`/etc/mqttbridge.conf`', for Windows '`.\mqttbridge\mqttbridge.conf`'). 

### Configuration
The configuation file is a JSON-formatted text with follows fields:

```json
[
  {
    "mqtt_host" : "192.168.1.32",
    "mqtt_port" : 1883,
    "topic_in" : "iot/out",
    "topic_out" : "iot/in",
    "rules_dir" : "Z:\\WI\\Lazarus\\mqttbridge\\.mqttbridge\\rules"
  }
]
```

+ [].mqtt_host			- name or IP-address of MQTT-brocker
+ [].mqtt_port			- port
+ [].rules_dir			- path to rules

+ [].autoexec[].period
+ [].autoexec[].cmd

### Rules

The rules are the thests with JSON-format. For Windows system they placed in the folder with executable application 
and subfolders `./.mqttbridge/rules/rules*.json`.

For Linux can be placed to any folder, but you need to append the path in the config file.

For example send our device following message:
```json
{"name":"IOT10241268","object":"dht","index":"4","temp":"19.3","humidity":"71.0","stype":"11","Battery":63.00}
```
and we have the rule
```json
{
   "src":{
      "mqtt_topic":"iot/out",
      "clauses":[
         {"path":"name","operation":"==","value":"IOT10241268"},
         {"path":"object","operation":"==","value":"dht"},
         {"path":"index","operation":"==","value":3}
      ],
      "op_clause":[
         {"clausea":0,"operation":"and","clauseb":1},
         {"clausea":1,"operation":"and","clauseb":2}
      ]
   },
   "dst":{
      "mqtt_topic":"domoticz/in",
      "actions":[
         {"action":"calcvalue","srcfieldvalue":"{temp};{humidity};0","dstfieldname":"svalue"},
         {"action":"calcvalue","srcfieldvalue":"{temp}","dstfieldname":"svalue1","dtype":"integer"},
         {"action":"insertfield","fieldname":"ID","fieldvalue":"1405D"},
         {"action":"insertfield","fieldname":"idx","fieldvalue":11},
         {"action":"copyfield","srcfieldname":"humidity","dstfieldname":"Humidity"},
         {"action":"copyfield","srcfieldname":"temp","dstfieldname":"Temp","dtype":"numeric"},
         {"action":"copyfield","srcfieldname":"Battery","dstfieldname":"Battery"}
      ]
   }
}
```
 
  
The outgoing message will have next data:

```json
{"svalue":"19.3;71.0;0","svalue1":19,"ID":"1405D","idx":11,"Humidity":71.0,"Temp":19.3,"Battery":63.0}
```

or
```json
{
   "src":{
      "mqtt_topic":"iot/out",
      "clauses":[
         {
            "path":"name",
            "operation":"==",
            "value":"TESTEXEC"
         }
      ],
      "op_clause":[
      ]
   },
   "dst":{
      "mqtt_topic":"domoticz/in",
      "actions":[
         {
            "action":"execute",
	    "srcempty":0,
	    "dstexecute":"curl \"http://ip.jsontest.com/\""
         }
      ]
   }
}
```

where

>> `src` - all clauses of incoming message;

>>> `mqtt_topic` - incoming topic of message;

>>> `clauses[].path` - the path to the fild for check into incomming message;

>>> `clauses[].operation` - must be "==";

>>> `clauses[].value` - the value in the filed of incomming message;

>> `dst` - all actions for outgoing message;

>>> `mqtt_topic` - outgoing topic;

>>> `actions[].action` - the kind of the action for outgoing message. It could have follows values: 

>>>> - `insertfield` - do the defined field with fixed value in outgoing message;
 
>>>> - `copyfield` - do copy of filed from incomming message into outgoing message (it could be with new fieldname in the outgoing message);
 
>>>> - `calcvalue` - do new field into otgoing message with values from diffrent other fileds of incomming message;
 
>>>> - `execute` - execute the command of command processor or shell and send (or not send) the result of command into the outgoing message;


### Command line switches

> `-h` help.

> `-r` or `--run` run as console application.

> `--add-conf` it started the dualog for build new configuration.

> `--service-install` register the apllication as a daemon on the Linux system. For window you can use the switches `-i` for install or `-r`for remove the service.

